// ==============================|| MENU ITEMS ||============================== //
// assets
import { UserOutlined } from '@ant-design/icons';

// icons
const icons = {
  UserOutlined
};

const menuItems = {
  items: [
    {
      id: 'group-user',
      title: 'Navigation',
      type: 'group',
      children: [
        {
          id: 'user',
          title: 'User Page',
          type: 'item',
          url: '/users',
          icon: icons.UserOutlined,
          breadcrumbs: false
        }
      ]
    }
  ]
};

export default menuItems;
